package com.test.spectrevpn;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.devs.vectorchildfinder.VectorChildFinder;
import com.devs.vectorchildfinder.VectorDrawableCompat;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.test.spectrevpn.ServerList.ServerListActivity;

import androidx.core.graphics.drawable.DrawableCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class HomeActivity extends AppCompatActivity{


    RelativeLayout button_main;
    ObjectAnimator objectanimator1 = null;

    Boolean checked = false;
    View switch_light;
    TextView status_text,connection_status;

ImageView image_btn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);





        button_main = (RelativeLayout) findViewById(R.id.button_main);
        switch_light = findViewById(R.id.switch_light);
        status_text = findViewById(R.id.status_text);
        connection_status = findViewById(R.id.connection_status);
        image_btn = findViewById(R.id.image_btn);

button_main.setOnTouchListener(new View.OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {


        int action = event.getActionMasked();
        switch (action){
            case MotionEvent.ACTION_UP:

                if (!checked) {
                    objectanimator1 = ObjectAnimator.ofFloat(button_main, "y", 230);
                    checked = true;
//                    Toast.makeText(MainActivity.this, "true", Toast.LENGTH_SHORT).show();

                    status_text.setText("Connecting");
                    connection_status.setText("Connecting");

                    button_main.setClickable(false);

                    new CountDownTimer(2000, 1000) {
                        public void onTick(long millisUntilFinished) {
                        }

                        public void onFinish() {

                            button_main.setClickable(true);

                            switch_light.setBackgroundColor(getResources().getColor(R.color.switch_light_color_on));
                            status_text.setText("ON");
                            connection_status.setText("Connected");


                            VectorChildFinder vector = new VectorChildFinder(HomeActivity.this, R.drawable.ic_button, image_btn);
                            VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
                            path1.setFillColor(Color.YELLOW);
                            VectorDrawableCompat.VFullPath path2 = vector.findPathByName("path2");
                            path2.setFillColor(Color.GREEN);

                        }
                    }.start();


                } else {
                    objectanimator1 = ObjectAnimator.ofFloat(button_main, "y", 20);
                    checked = false;
//                    Toast.makeText(MainActivity.this, "false", Toast.LENGTH_SHORT).show();

                    switch_light.setBackgroundColor(getResources().getColor(R.color.switch_light_color_off));
                    status_text.setText("OFF");
                    connection_status.setText("Disconnect");


                    image_btn.setImageResource(R.drawable.ic_button);
                }


                objectanimator1.setDuration(200);
                objectanimator1.start();

                return true;
        }



        return false;
    }
});


        button_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });


        findViewById(R.id.select_country).setOnClickListener(v -> {
            startActivity(new Intent(HomeActivity.this, ServerListActivity.class));
        });


    }


}