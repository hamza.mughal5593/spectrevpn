package com.test.spectrevpn.Model;

public class CountryListModel {
    int id;
    int country_icon;
    String country_name;
    boolean isSelected ;

    public CountryListModel(int id, int country_icon, String country_name, boolean isSelected) {
        this.id = id;
        this.country_icon = country_icon;
        this.country_name = country_name;
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getId() {
        return id;
    }

    public int getCountry_icon() {
        return country_icon;
    }

    public String getCountry_name() {
        return country_name;
    }
}
