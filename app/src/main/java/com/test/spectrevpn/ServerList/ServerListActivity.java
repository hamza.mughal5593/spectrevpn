package com.test.spectrevpn.ServerList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Window;
import android.widget.EditText;

import com.test.spectrevpn.Adatper.CountryListAdapter;
import com.test.spectrevpn.Model.CountryListModel;
import com.test.spectrevpn.R;

import java.util.ArrayList;

public class ServerListActivity extends AppCompatActivity implements CountryListAdapter.OnItemClickListener {
    ArrayList<CountryListModel> countryList = new ArrayList<>();
    CountryListAdapter countryListAdapter;
    RecyclerView recyclr;

    EditText search_country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_server_list);


        recyclr = findViewById(R.id.recyclr);
        search_country = findViewById(R.id.search_country);


        countryList.add(new CountryListModel(1, R.id.country_icon, "United Estates", false));
        countryList.add(new CountryListModel(2, R.id.country_icon, "Canada", false));
        countryList.add(new CountryListModel(3, R.id.country_icon, "Australia", false));
        countryList.add(new CountryListModel(4, R.id.country_icon, "America", false));
        countryList.add(new CountryListModel(5, R.id.country_icon, "Germany", false));
        countryListAdapter = new CountryListAdapter(ServerListActivity.this, countryList, this);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1, RecyclerView.VERTICAL, false);
        recyclr.setLayoutManager(layoutManager);
        recyclr.setAdapter(countryListAdapter);


        search_country.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                filter(s.toString());

                countryListAdapter.filter(s.toString());

            }
        });


    }


    @Override
    public void onItemClicked(CountryListModel server, int postion, boolean isSelected) {


        for (int i = 0; i < countryList.size(); i++) {
            countryList.get(i).setSelected(false);
        }

        countryList.get(postion).setSelected(true);

        countryListAdapter.notifyDataSetChanged();

    }


}